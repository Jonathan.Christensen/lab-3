package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    
    @Override
    public String run(String[] args) {
        String newLine = ""; 
        for (String string : args) {
            newLine += string + " ";
        }
        return newLine;
    }

    @Override
    public String getName() {
        return "echo";
    }
    
}
